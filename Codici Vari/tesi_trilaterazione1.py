import json
import math
import random
import numpy as np

#trilaterazione base

"""
#chiedo le posizioni
X=[]
Y=[]
D=[]

j=0
c='s'
while c=="s":
    i=0
    X.append([])
    Y.append([])
    D.append([])
    while i<3:
        X[j].append(float(input('Centro '+ str(i) +' con coordinata X: ')))
        Y[j].append(float(input('Centro '+ str(i) +' con coordinata Y: ')))
        D[j].append(float(input("Centro "+ str(i) +" con distanza dall'emettitore: ")))
        i=i+1
    j=j+1
    c = input('Vuoi inserire altri dati (s/n)? ')

i=0
while i<j:
    try:
        y=-1*((X[i][1]-X[i][2])*((pow(X[i][1],2)-pow(X[i][0],2))+(pow(Y[i][1],2)-pow(Y[i][0],2))+(pow(D[i][0],2)-pow(D[i][1],2)))-(X[i][0]-X[i][1])*((pow(X[i][2],2)-pow(X[i][1],2))+(pow(Y[i][2],2)-pow(Y[i][1],2))+(pow(D[i][1],2)-pow(D[i][2],2))))/(2*((Y[i][0]-Y[i][1])*(X[i][1]-X[i][2])-(Y[i][1]-Y[i][2])*(X[i][0]-X[i][1])))
        x=-1*((Y[i][1]-Y[i][2])*((pow(Y[i][1],2)-pow(Y[i][0],2))+(pow(X[i][1],2)-pow(X[i][0],2))+(pow(D[i][0],2)-pow(D[i][1],2)))-(Y[i][0]-Y[i][1])*((pow(Y[i][2],2)-pow(Y[i][1],2))+(pow(X[i][2],2)-pow(X[i][1],2))+(pow(D[i][1],2)-pow(D[i][2],2))))/(2*((X[i][0]-X[i][1])*(Y[i][1]-Y[i][2])-(X[i][1]-X[i][2])*(Y[i][0]-Y[i][1])))
        print('X: ' + str(x) + ' & Y: ' + str(y))
    except ZeroDivisionError:
        print("Sicuro che i 3 centri siano distinti?")
    i=i+1
"""

#trilaterazione finale

#importo dati
dati = json.load(open('dati_final_t.json'))

massimo=len(dati['X'])
#massimo=massimo-massimo%3

#prendo i dati
Xstr=dati['X']
Ystr=dati['Y']
Dstr=dati['DistanzaMisurata']

#metto i dati in insiemi di 3, in varie combinazioni così da avere più combinazioni da trilaterare
X=[]
Y=[]
D=[]

j=0
c='s'
while j<massimo:
    i=0
    k=random.randint(0,massimo-3)
    X.append([])
    Y.append([])
    D.append([])
    while i<3:
        X[j].append(float(Xstr[k]))
        Y[j].append(float(Ystr[k]))
        D[j].append(float(Dstr[k]))
        k=k+1
        i=i+1
    j=j+1
    
xC=[]
yC=[]
erroreC=[]
             
i=0
while i<j:
    try:
        y=-1*((X[i][1]-X[i][2])*((pow(X[i][1],2)-pow(X[i][0],2))+(pow(Y[i][1],2)-pow(Y[i][0],2))+(pow(D[i][0],2)-pow(D[i][1],2)))-(X[i][0]-X[i][1])*((pow(X[i][2],2)-pow(X[i][1],2))+(pow(Y[i][2],2)-pow(Y[i][1],2))+(pow(D[i][1],2)-pow(D[i][2],2))))/(2*((Y[i][0]-Y[i][1])*(X[i][1]-X[i][2])-(Y[i][1]-Y[i][2])*(X[i][0]-X[i][1])))
        yC.append(y)
        x=-1*((Y[i][1]-Y[i][2])*((pow(Y[i][1],2)-pow(Y[i][0],2))+(pow(X[i][1],2)-pow(X[i][0],2))+(pow(D[i][0],2)-pow(D[i][1],2)))-(Y[i][0]-Y[i][1])*((pow(Y[i][2],2)-pow(Y[i][1],2))+(pow(X[i][2],2)-pow(X[i][1],2))+(pow(D[i][1],2)-pow(D[i][2],2))))/(2*((X[i][0]-X[i][1])*(Y[i][1]-Y[i][2])-(X[i][1]-X[i][2])*(Y[i][0]-Y[i][1])))
        xC.append(x)
        errore=math.sqrt(pow((x-0),2)+pow((y-0),2))
        erroreC.append(errore)
        print('X: ' + str(x) + ' & Y: ' + str(y) + ' & Errore: ' + str(errore))
    except ZeroDivisionError:
        print("Sicuro che i 3 centri siano distinti?")
    i=i+1

errore_medio=sum(erroreC)/len(erroreC)
varianza_errore=np.var(erroreC)
print()
print(errore_medio)
print(varianza_errore)

centri={ 'Y':yC,
         'X':xC,
         'errore':erroreC,
         'erroreMedio':errore_medio,
         'varianzaErrore':varianza_errore
    }

#salvo il dict come json
with open('centri_trilaterati.json', 'w') as fp:
    json.dump(centri, fp)
