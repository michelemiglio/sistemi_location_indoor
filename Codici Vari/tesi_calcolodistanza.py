import json
import math

#importo dati
dati = json.load(open('dati_test_tutti.json'))

massimo=len(dati['X'])

dist_geometrica=[]
dist_misurata=[]
txpower=-57.21933007402491
n=2

i=0

while i<massimo:
    #calcolo la distanza geometrica dalle coordinate
    dist_geometrica.append(math.sqrt(pow(float(dati['X'][i]),2)+pow(float(dati['Y'][i]),2)))
    #calcolo la distanza dall'rssi
    dist_misurata.append(pow(((txmpower-float(dati['RSSI'][i]))/(10*n)),10))
    errore.append(dist_misurata[i]-dist_geometrica[i])
    i=i+1
    
print(dist_calcolata);

datiPiuDistanza={'X':dati['X'],
                 'Y':dati['Y'],
                 'RSSI':dati['RSSI'],
                 'DistanzaGeometrica':dist_geometrica,
                 'DistanzaMisurata':dist_misurata,
                 'Errore':errore
            }
#salvo il dict come json
with open('datiPiuDistanza.json', 'w') as fp:
        json.dump(datiPiuDistanza, fp)
