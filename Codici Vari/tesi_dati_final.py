import subprocess
import json
import math

#dati
X=[]
Y=[]
rssi=[]
dist_geometrica=[]
dist_misurata=[]
errore=[]
txpower=-57.21933007402491
n=2
i=0

#continuare a chiedere dati?
c="s"

while c=="s":
        X.append(input('Inserisci la coordinata X: '))
        Y.append(input('Inserisci la coordinata Y: '))

        #misuro l'rssi con sudo btmgmt find | grep -m 1 '2C:41:A1:E1:39:83' | cut -c55-57
        process = subprocess.run(['sudo btmgmt find | grep -m 1 "2C:41:A1:E1:39:83" | cut -c55-57'], stdin=None, input=None, stdout=subprocess.PIPE, stderr=None, capture_output=False, shell=True, cwd=None, timeout=None, check=True, encoding=None, errors=None, text=None, env=None, universal_newlines=True)
        rssi.append(process.stdout.replace("\n", ""))
        #calcolo la distanza geometrica dalle coordinate
        dist_geometrica.append(math.sqrt(pow(float(X[i]),2)+pow(float(Y[i]),2)))
        print(dist_geometrica[i])
        #calcolo la distanza dall'rssi
        dist_misurata.append(pow(((txpower-float(rssi[i]))/(10*n)),10))
        print(dist_misurata[i])
        errore.append(dist_misurata[i]-dist_geometrica[i])
        print(errore[i])
        print()
        
        i=i+1
        c = input('Vuoi inserire altri dati (s/n)? ')

dati = {'X':X,
        'Y':Y,
        'RSSI':rssi,
        'DistanzaGeometrica':dist_geometrica,
        'DistanzaMisurata':dist_misurata,
        'Errore':errore}

print(dati)

#salvo la matrice come csv
with open('dati_final.json', 'w') as fp:
        json.dump(dati, fp)
