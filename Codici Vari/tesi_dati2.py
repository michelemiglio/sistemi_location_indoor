import subprocess
import json

#dati
X=[]
Y=[]
rssi=[]

#continuare a chiedere dati?
c="s"

while c=="s":
        X.append(input('Inserisci la coordinata X: '))
        Y.append(input('Inserisci la coordinata Y: '))

        #misuro l'rssi con sudo btmgmt find | grep -m 1 '2C:41:A1:E1:39:83' | cut -c55-57
        process = subprocess.run(['sudo btmgmt find | grep -m 1 "2C:41:A1:E1:39:83" | cut -c55-57'], stdin=None, input=None, stdout=subprocess.PIPE, stderr=None, capture_output=False, shell=True, cwd=None, timeout=None, check=True, encoding=None, errors=None, text=None, env=None, universal_newlines=True)
        rssi.append(process.stdout.replace("\n", ""))
        #rssi.append('prova')
        c = input('Vuoi inserire altri dati (s/n)? ')

dati = {'X':X,'Y':Y,'RSSI':rssi}

print(dati)

#salvo la matrice come csv
with open('dati_salotto_x.json', 'w') as fp:
        json.dump(dati, fp)