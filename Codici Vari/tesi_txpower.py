import json
import math

#importo dati
dati = json.load(open('dati_final_t.json'))

massimo=len(dati['X'])

dist_calcolata=[]
dist_misurata={0.5: [], 1.0: [], 1.5: [], 2.0: [], 2.5: [], 3.0: [], 3.5: [], 4.0: [], 4.5: [], 5.0: []}
errore={0.5: [], 1.0: [], 1.5: [], 2.0: [], 2.5: [], 3.0: [], 3.5: [], 4.0: [], 4.5: [], 5.0: []}
txpower={0.5: [], 1.0: [], 1.5: [], 2.0: [], 2.5: [], 3.0: [], 3.5: [], 4.0: [], 4.5: [], 5.0: []}
meaner ={}
txmedio={}

#per tarare il txpower uso n
n=0.5

i=0

while n<5.5:
    while i<massimo:
        #calcolo la distanza dalle coordinate
        dist_calcolata.append(math.sqrt(pow(float(dati['X'][i]),2)+pow(float(dati['Y'][i]),2)))
        #calcolo il txpower
        txpower[n].append(math.log10(dist_calcolata[i])*10*n+float(dati['RSSI'][i]))
        i=i+1
    txmedio[n]=sum(txpower[n])/len(txpower[n])
    print("N = " + str(n) + " e Tx = " + str(txmedio[n]))
    n=n+0.5
    i=0

n=0.5
while n<5.5:
    while i<massimo:
        #calcolo la distanza dall'rssi
        dist_misurata[n].append(pow(((txmedio[n]-float(dati['RSSI'][i]))/(10*n)),10))
        #calcolo l'errore
        errore[n].append(dist_calcolata[i]-dist_misurata[n][i])
        #salvo l'errore per i vari n
        
        i=i+1
    meaner[n]= sum(errore[n])/len(errore[n])
    n=n+0.5
    i=0
    
print(meaner);

stima_tx_n={'0.5':[txmedio[0.5],meaner[0.5]],
            '1':[txmedio[1],meaner[1]],
            '1.5':[txmedio[1.5],meaner[1.5]],
            '2':[txmedio[2],meaner[2]],
            '2.5':[txmedio[2.5],meaner[2.5]],
            '3':[txmedio[3],meaner[3]],
            '3.5':[txmedio[3.5],meaner[3.5]],
            '4':[txmedio[4],meaner[4]],
            '4.5':[txmedio[4.5],meaner[4.5]],
            '5':[txmedio[5],meaner[5]],
            }
#salvo il dict come json
with open('stima_tx_n.json', 'w') as fp:
        json.dump(stima_tx_n, fp)
